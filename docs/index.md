# Nibi widget overview

#### Version: 1.0
####  Last updated: 11 October 2018


Nibi widget is a embeddable component that works independently in your webpage. Nibi widget is a ReactJs component that works extremely fast in comparison to  other(AngularJs, emberjs) because of ReactJs virtual DOM and Javascript driven templates.

In order to use the widget you need to get a access token that expires in 24 hrs. NIbi widget is going to use Access token only that is temporary token that means nobody can use this token more than 24hrs. To get the serve side access token you need to have API_KEY that is safe and not visible to user on client. 

The advantage of using widget that you can customise it completely by importing your css. Nibi widget has a prefix css class `.nibi-widget` that can be used to extend styling/theme of widget.

An example of theme customization
```
.nibi-widget .message-container{
  background-color : ‘#ccc’
}
```
In the same way, color, font-size can be changed.


## ** JavaScript/Css  installation **
```
<link rel="stylesheet" href="https://ask.nibi.ai/widget/nibi-ai-widget.v1.css" />
<script type="text/javascript" src="https://ask.nibi.ai/widget/nibi-ai-widget.v1.js"></script>
``` 

To enable the basic widget, add the following to your page before closing <body> tag:

<div id="widget"  ></div>
```
<script>
NIBI_AI.config({
 API_KEY: '', // This is access token.
 DATASOURCE_NAME: '' // DATASOURCE_NAME - required
});
var widget = NIBI_AI.widget.autoComplete.new({
 selector: "#widget",
 placement : ‘TOP|BOTTOM’
});
widget.render();
</script>
```
** API_KEY ** -> This is required property. You can get this property by login in your account and Developer Section(we will have this section in admin account. User can see this in his dashboard)

** DATASOURCE_NAME ** -> This is required property. You can get source name by login in your account and sources list.


_Widget is not going to show anything if API_KEY and DATASOURCE_NAME are not entered and valid._

_Note - Your widget is ready to use now._

Generate API Key

Login to your account in admin and click on Developer settings. Please refer to below screen
![alt text](./dev-seeting.png "Dev Setting")
 
Once you click on `add new`, you will be asked for API key name
![alt text](./create-api-key.png "create api key")


Click on `Create`
![alt text](./create.png "create api key")

 
We highly recommend you to keep API key secure and safe. We do not regenerate this API key. 


## ** Get Access token Server Side **

To get the access token you must have an valid API key. Due to security reasons, we ask developer to get access token on server side. Once you get access token that is valid for next 24hrs. Refer to the screen in the admin. 
Server side Example of 


We have written an example in PHP,  You can use any server side language [python|node|java etc]


```
<?php 
class NibiAuth {
protected $API_KEY = 'YOUR API TOKEN; // API Key that you generate from Admin.

protected $API_URL = 'https://ask.nibi.ai/api/admin/v1/api_token/get_user_token';
  public function getAccessToken() {
       $ch = curl_init($this->API_URL);
       curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
       curl_setopt($ch, CURLOPT_POSTFIELDS, 0);
       curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
       curl_setopt($ch, CURLOPT_HTTPHEADER,
           array(
               'Authorization: Bearer ' . $this->API_KEY,
           ));
       curl_setopt($ch, CURLOPT_TIMEOUT, 5);
       curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
       $response = curl_exec($ch);
       curl_close($ch);
       return $this->responseHandler($response);
   }

   private function responseHandler($response)
   {
       $result = json_decode($response, true);
       if (!is_object($result) && !is_array($result)) {
           return '';
       }
       return isset($result['access_token']) ? $result['access_token'] : '';
   }


}
$auth =  new NibiAuth();
$token = $auth->getAccessToken();
?>
```
## ** Advanced Configuration **

#### ** Properties **
Property  | Optional/Required | Description
------------ | ------------- | -------------
selector | Required | An id of HTML element where widget is rendered
placement | Optional **[TOP/BOTTOM]** | Default `TOP` A position of autocomplete to display from top or bottom of input field
countOfResults | Optional | Default - 5 Number of records to be fetched
addBookmark | Optional | Default - true Enables `Add bookmark` button
showBookmark | Optional | Default - true Enables `show bookmark` button
showHistory | Optional | Default - true Enables `show history` button
autoCompleteWord | Optional | Default - true Enables keywords/words suggestions that includes (schema fields name, and keywords like how many , that are, of etc)
autoCompleteSentense | Optional | Default - true Enables autocomplete suggestions based on your `query text`
containerHeight | Optional | Default - 400px Apply max-height to autocomplete container. If height of suggestions increased more than given height `scrolling` on container is enabled.
events | Optional | Events that are triggered on specific actions by widget and callbacks are trigger with data. Please see events doc below

#### ** Events  **
Event  | Optional/Required | Description
------------ | ------------- | -------------
onInit | **Optional** - onInit(datasource:any) | Triggers when widget is mounted. `datasource`  - an Json Object of datasource which is in widget.
onTyping | **Optional** - onTyping(npl: string, suggestions: Array) | Triggers when typing. npl - a question that is being typed in input suggestions - An array of suggestions based of npl
OnSearch | Optional - onSearch(result: any) | Triggers when question is searched. result - A response of NPL that contains results, SQL query
beforeAskQuestion | Optional - ** beforeAskQuestion(question: string)** | Triggers before a request is made to Server to get the result of question. question - returns a question string

### *** Example of events ***

// lets create an instance of autocomplete widget
```


const widget = new NIBI_AI.autocomplete({
 selector: "#INPUT_FIELD_ID",
 placement : ‘TOP|BOTTOM’, 
 events: {
   onInit: () => {
     // event is triggered when widget instance is created
   },
   onTyping:   (npl: string, suggestions: Array) => {
     //event is triggered when data is typed and returns whole entered NPL string
     // element of array {"full":"How many companies that","suggestion":"that","meta":{}}  
   },
   onSearch: (result: any) => {
     //event is triggered when question is asked and returns result from API
   }
 }
});
```

## ** Full Example of Usage - **

```
<?php

class NibiAuth
{
   protected $API_KEY = 'YOUR API_TOKEN';
   protected $API_URL = 'https://ask.nibi.ai/api/admin/v1/api_token/get_user_token';


   public function getAccessToken()
   {
       $ch = curl_init($this->API_URL);
       curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
       curl_setopt($ch, CURLOPT_POSTFIELDS, 0);
       curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
       curl_setopt($ch, CURLOPT_HTTPHEADER,
           array(
               'Authorization: Bearer ' . $this->API_KEY,
           ));
       curl_setopt($ch, CURLOPT_TIMEOUT, 5);
       curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
       $response = curl_exec($ch);
       curl_close($ch);
       return $this->responseHandler($response);
   }

   private function responseHandler($response)
   {
       $result = json_decode($response, true);
       if (!is_object($result) && !is_array($result)) {
           return '';
       }
       return isset($result['access_token']) ? $result['access_token'] : '';
   }
}

$auth = new NibiAuth();
$token = $auth->getAccessToken();
?>
<!DOCTYPE html>
<html lang="en" class="default-style">
<head>
   <title>Source Configuration</title>
   <script>
       var api_token = '<?=$token?>';
   </script>
   <script type="text/javascript" src="http://localhost/tomer/nibi_widget/build/nibi-ai-widget.v1.js"></script>
   <link rel="stylesheet" href="https://ask.nibi.ai/widget/nibi-ai-widget.v1.css"/>

</head>
<body>

<div id="widget"></div>
<div id="widget-output"></div>

<script>
   /**
    * configure NIBI_AI widget
    * */
   NIBI_AI.config({
       API_KEY: api_token, // API_KEY is same as access token
       DATASOURCE_NAME: ‘’// DATASOURCE_NAME
   })
   ;
   /**
    * Create Instance widget
    * */
   var widget = NIBI_AI.widget.autoComplete.new({
       selector: "#widget",
       placement: 'TOP',
       events: {
            onSearch: (result) => {
               document.getElementById('widget-output').value = JSON.stringify(result);
           }
       }
   });
   widget.render();

</script>
</body>
</html>
```

