Nibi Documentation

### How do I get set up? ###

* Clone Repo - `git clone https://singhmohancs@bitbucket.org/nibi_ai/nibi_docs.git`
* cd nibi_docs
* pip install mkdocs


## Development
`mkdocs serve`


## Production code
`mkdocs build`

This generates `site` folder that is ready to deploy on Production



### Who do I talk to? ###

* Mohan Singh<mslogicmaster@gmail.com>